#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `org_todo_metrics` package."""

from datetime import datetime, timedelta

import pytest
from click.testing import CliRunner
from org_todo_metrics import cli
from org_todo_metrics.org_todo_metrics import (
    StateTransition,
    all_closed_todos,
    all_org_nodes,
    average_time_spent_working,
    delta,
    get_mean_time_to_completion,
    get_work_tasks,
    mean_timedelta,
    real_time_took,
    state_transition_filter,
    get_made_todo_timestamp,
)
from orgparse import loads


@pytest.mark.parametrize(
    "state_transitions,date_time",
    [
        [
            [
                StateTransition(
                    work_task_string='- State "TODO"    from              [2019-10-26 Sat 21:00]'
                ),
                StateTransition(
                    work_task_string='- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]'
                ),
                StateTransition(
                    work_task_string='- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]'
                ),
            ],
            datetime(2019, 10, 26, 21, 0),
        ]
    ],
)
def test_get_made_todo_timestamp(state_transitions, date_time):
    todo_timestamp = get_made_todo_timestamp(state_transitions)
    assert todo_timestamp == date_time


@pytest.mark.parametrize(
    "work_task,time_took",
    [
        [
            [
                '- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
                '- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
                '- State "TODO"    from              [2019-10-26 Sat 21:00]',
            ],
            [
                '- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
                '- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
                '- State "TODO"    from              [2019-10-26 Sat 21:00]',
            ],
        ],
        [
            ['- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]'],
            ['- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]'],
        ],
        [
            ['- State "TODO"    from              [2019-10-26 Sat 21:00]'],
            ['- State "TODO"    from              [2019-10-26 Sat 21:00]'],
        ],
        [
            ['- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]'],
            ['- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]'],
        ],
        [
            [
                "* DONE get groceries",
                "CLOSED: [2019-10-26 Sat 21:01]",
                '- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
                '- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
                '- State "TODO"    from              [2019-10-26 Sat 21:00]',
            ],
            [
                '- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
                '- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
                '- State "TODO"    from              [2019-10-26 Sat 21:00]',
            ],
        ],
        [
            [
                "* DONE get groceries",
                "CLOSED: [2019-10-26 Sat 21:01]",
                '     - State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
                '     - State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
                '     - State "TODO"    from              [2019-10-26 Sat 21:00]',
            ],
            [
                '     - State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
                '     - State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
                '     - State "TODO"    from              [2019-10-26 Sat 21:00]',
            ],
        ],
    ],
)
def test_state_transition_filter(work_task, time_took):
    state_tranistitions = state_transition_filter(work_task)
    work_task_strings = [
        state_tranistition.work_task_string
        for state_tranistition in state_tranistitions
    ]
    for item in time_took:
        assert item in work_task_strings


@pytest.mark.parametrize(
    "work_task,time_took",
    [
        [
            [
                "* DONE get groceries",
                "CLOSED: [2019-10-26 Sat 21:01]",
                '- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
                '- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
                '- State "TODO"    from              [2019-10-26 Sat 21:00]',
            ],
            timedelta(seconds=300),
        ],
        [
            [
                "* DONE get groceries",
                "CLOSED: [2019-10-26 Sat 21:01]",
                ' - State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
                '- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
                '- State "TODO"    from              [2019-10-26 Sat 21:00]',
            ],
            timedelta(seconds=300),
        ],
        [
            [
                "* DONE get groceries",
                "CLOSED: [2019-10-26 Sat 21:01]",
                '- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:02]',
                '- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:00]',
                '- State "TODO"    from              [2019-10-26 Sat 21:00]',
            ],
            timedelta(seconds=120),
        ],
        [
            [
                "* DONE test",
                "  CLOSED: [2019-10-28 Mon 03:06]",
                '   - State "DONE"     from "IN PROGRESS" [2019-10-28 Mon 03:07]',
                '   - State "IN PROGRESS" from "TODO"    [2019-10-28 Mon 03:06]',
                '   - State "TODO"    from              [2019-10-28 Mon 03:06]',
            ],
            timedelta(seconds=60),
        ],
        [
            [
                "* DONE test",
                "  CLOSED: [2019-10-28 Mon 03:06]",
                '   - State "DONE"     from "IN PROGRESS" [2019-10-28 Mon 03:07]',
                '   - State "IN PROGRESS" from "TODO"    [2019-10-28 Mon 03:06]',
                '    - State "TODO"    from              [2019-10-28 Mon 03:06]',
            ],
            timedelta(seconds=60),
        ],
    ],
)
def test_real_time_took(work_task, time_took):
    assert real_time_took(work_task) == time_took


@pytest.mark.parametrize(
    "time_deltas,expected",
    [
        [
            [
                timedelta(seconds=10),
                timedelta(seconds=10),
                timedelta(seconds=10),
                timedelta(seconds=10),
                timedelta(seconds=10),
                timedelta(seconds=10),
            ],
            timedelta(seconds=10),
        ],
        [[timedelta(seconds=10), timedelta(seconds=10)], timedelta(seconds=10)],
    ],
)
def test_mean_timedelta(time_deltas, expected):
    assert mean_timedelta(time_deltas) == expected


@pytest.mark.parametrize(
    "work_task, expected_meant_time_to_completion",
    [
        [
            """
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]
- State "TODO"    from              [2019-10-26 Sat 21:00]""",
            240,
        ],
        [
            """
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]
- State "TODO"    from              [2019-10-26 Sat 21:00]
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]
- State "TODO"    from              [2019-10-26 Sat 21:00]
""",
            240,
        ],
        [
            """
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:02]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:00]
- State "TODO"    from              [2019-10-26 Sat 21:00]
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:04]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:00]
- State "TODO"    from              [2019-10-26 Sat 21:00]
""",
            180,
        ],
        [
            """
* DONE test
  CLOSED: [2019-10-28 Mon 03:06]
    - State "DONE"     from "IN PROGRESS" [2019-10-28 Mon 03:07]
    - State "IN PROGRESS" from "TODO"    [2019-10-28 Mon 03:06]
    - State "TODO"    from              [2019-10-28 Mon 03:06]
            """,
            60,
        ],
    ],
)
def test_average_time_spent_working(work_task, expected_meant_time_to_completion):
    mean_time_to_completion = average_time_spent_working(work_task)
    assert mean_time_to_completion == expected_meant_time_to_completion


@pytest.mark.parametrize(
    "work_task, expected_meant_time_to_completion",
    [
        [
            """
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]
- State "TODO"    from              [2019-10-26 Sat 21:00]""",
            240,
        ],
        [
            """
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]
- State "TODO"    from              [2019-10-26 Sat 21:00]
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]
- State "TODO"    from              [2019-10-26 Sat 21:00]
""",
            240,
        ],
        [
            """
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:02]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:00]
- State "TODO"    from              [2019-10-26 Sat 21:00]
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:04]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:00]
- State "TODO"    from              [2019-10-26 Sat 21:00]
""",
            180,
        ],
        [
            """
* DONE test
  CLOSED: [2019-10-28 Mon 03:06]
    - State "DONE"     from "IN PROGRESS" [2019-10-28 Mon 03:07]
    - State "IN PROGRESS" from "TODO"    [2019-10-28 Mon 03:06]
    - State "TODO"    from              [2019-10-28 Mon 03:06]
            """,
            60,
        ],
    ],
)
def test_get_mean_time_to_completion(work_task, expected_meant_time_to_completion):
    org_tree = loads(work_task)
    closed_todos = all_closed_todos(org_tree=org_tree)
    mean_time_to_completion = get_mean_time_to_completion(closed_todos)
    assert mean_time_to_completion == expected_meant_time_to_completion


@pytest.mark.parametrize(
    "work_task_string_1,work_task_string_2,expected_delta",
    [
        [
            '- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
            '- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
            timedelta(seconds=240),
        ],
        [
            '    - State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
            '    - State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
            timedelta(seconds=240),
        ],
    ],
)
def test_Work_Task_Pair_delta(work_task_string_1, work_task_string_2, expected_delta):
    """
    """
    work_task_1 = StateTransition(work_task_string=work_task_string_1)
    work_task_2 = StateTransition(work_task_string=work_task_string_2)
    work_task_pair = delta(first=work_task_1, second=work_task_2)
    assert work_task_pair == expected_delta


@pytest.mark.parametrize(
    "work_task,timestamp",
    [
        [
            '- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
            datetime(year=2019, month=10, day=26, hour=21, minute=5),
        ],
        [
            '- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
            datetime(year=2019, month=10, day=26, hour=21, minute=1),
        ],
        [
            '    - State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
            datetime(year=2019, month=10, day=26, hour=21, minute=5),
        ],
        [
            '    - State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
            datetime(year=2019, month=10, day=26, hour=21, minute=1),
        ],
    ],
)
def test_Work_Task_timestamp(work_task, timestamp):
    work_task = StateTransition(work_task_string=work_task)
    assert work_task.timestamp == timestamp
    assert work_task.timestamp.date() == timestamp.date()
    assert work_task.timestamp.time() == timestamp.time()


@pytest.mark.parametrize(
    "work_task,from_state",
    [
        ['- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]', "IN PROGRESS"],
        ['- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]', "TODO"],
        [
            '    - State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]',
            "IN PROGRESS",
        ],
        ['    - State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]', "TODO"],
        ['    - State "IN PROGRESS" from           [2019-10-26 Sat 21:01]', None],
        ['- State "TODO"    from              [2019-10-26 Sat 21:00]', None],
    ],
)
def test_Work_Task_from_state(work_task, from_state):
    work_task = StateTransition(work_task_string=work_task)
    assert work_task.from_state == from_state


@pytest.mark.parametrize(
    "work_task,begin_state",
    [
        ['- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]', "DONE"],
        ['- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]', "IN PROGRESS"],
        ['    - State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]', "DONE"],
        [
            '    - State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]',
            "IN PROGRESS",
        ],
    ],
)
def test_Work_Task_begin_state(work_task, begin_state):
    work_task = StateTransition(work_task_string=work_task)
    assert work_task.to_state == begin_state


@pytest.mark.parametrize(
    "work_task_fixture",
    [
        """
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]
- State "TODO"    from              [2019-10-26 Sat 21:00]
        """,
        """
* DONE test
  CLOSED: [2019-10-28 Mon 03:06]
    - State "DONE"     from "IN PROGRESS" [2019-10-28 Mon 03:07]
    - State "IN PROGRESS" from "TODO"    [2019-10-28 Mon 03:06]
    - State "TODO"    from              [2019-10-28 Mon 03:06]
        """,
    ],
)
def test_get_work_task(work_task_fixture):
    org_tree = loads(work_task_fixture)
    node_list = all_org_nodes(org_tree)
    node = node_list[0]
    work_tasks = get_work_tasks(node)
    assert len(work_tasks) == 2


@pytest.mark.parametrize(
    "var,node_length",
    [
        [
            """
* Node 1
** Node 2""",
            0,
        ],
        [
            """
* Node 1
** Node 2
* Node3
** Node 4
*** Node 5
** Node 6""",
            0,
        ],
        [
            """
* Node 1
  CLOSED: [2012-02-26 Sun 21:15]
** Node 2
* Node3
** Node 4
*** Node 5
** Node 6""",
            1,
        ],
        [
            """
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]
- State "TODO"    from              [2019-10-26 Sat 21:00]""",
            1,
        ],
        [
            """
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]
- State "TODO"    from              [2019-10-26 Sat 21:00]
""",
            1,
        ],
        [
            """
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:02]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:00]
- State "TODO"    from              [2019-10-26 Sat 21:00]
""",
            1,
        ],
        [
            """
* DONE test
  CLOSED: [2019-10-28 Mon 03:06]
    - State "DONE"     from "IN PROGRESS" [2019-10-28 Mon 03:07]
    - State "IN PROGRESS" from "TODO"    [2019-10-28 Mon 03:06]
    - State "TODO"    from              [2019-10-28 Mon 03:06]
            """,
            1,
        ],
    ],
)
def test_all_closed_todos_org_node_list(var, node_length):
    """
    test that all_closed_todoes only returns todos that have been closed
    """
    org_tree = loads(var)
    node_list = all_org_nodes(org_tree)
    closed_todo_nodes = all_closed_todos(org_node_list=node_list)
    assert len(closed_todo_nodes) == node_length


@pytest.mark.parametrize(
    "var,node_length",
    [
        [
            """
* Node 1
** Node 2""",
            0,
        ],
        [
            """
* Node 1
** Node 2
* Node3
** Node 4
*** Node 5
** Node 6""",
            0,
        ],
        [
            """
* Node 1
  CLOSED: [2012-02-26 Sun 21:15]
** Node 2
* Node3
** Node 4
*** Node 5
** Node 6""",
            1,
        ],
    ],
)
def test_all_closed_todos_org_tree(var, node_length):
    """
    test that all_closed_todoes only returns todos that have been closed
    """
    org_tree = loads(var)
    node_list = all_closed_todos(org_tree=org_tree)
    assert len(node_list) == node_length


@pytest.mark.parametrize(
    "var,node_length",
    [
        [
            """
* Node 1
** Node 2 """,
            2,
        ],
        [
            """
* Node 1
** Node 2
* Node3
** Node 4
*** Node 5
** Node 6 """,
            6,
        ],
    ],
)
def test_all_org_nodes(var, node_length):
    """
    test that all_org_nodes returns all all org nodes in a tree
    """
    org_tree = loads(var)
    node_list = all_org_nodes(org_tree)
    assert len(node_list) == node_length


# def test_command_line_interface():
#     """Test the CLI."""
#     runner = CliRunner()
#     result = runner.invoke(cli.main)
#     assert result.exit_code == 0
#     assert 'org_todo_metrics.cli.main' in result.output
#     help_result = runner.invoke(cli.main, ['--help'])
#     assert help_result.exit_code == 0
#     assert '--help  Show this message and exit.' in help_result.output


@pytest.mark.parametrize(
    "org_string,expected_output,exit_code",
    [
        [
            """
* DONE get groceries
CLOSED: [2019-10-26 Sat 21:01]
- State "DONE"     from "IN PROGRESS" [2019-10-26 Sat 21:05]
- State "IN PROGRESS" from "TODO"    [2019-10-26 Sat 21:01]
- State "TODO"    from              [2019-10-26 Sat 21:00]
            """,
            "Mean Time to Completion in seconds: 240",
            0,
        ],
        [
            """
* DONE test
CLOSED: [2019-10-28 Mon 03:06]
- State "DONE"     from "IN PROGRESS" [2019-10-28 Mon 03:07]
- State "IN PROGRESS" from "TODO"    [2019-10-28 Mon 03:06]
- State "TODO"    from              [2019-10-28 Mon 03:06]
            """,
            "Mean Time to Completion in seconds: 60",
            0,
        ],
        [
            """
* DONE test
  CLOSED: [2019-10-28 Mon 03:06]
    - State "DONE"     from "IN PROGRESS" [2019-10-28 Mon 03:07]
    - State "IN PROGRESS" from "TODO"    [2019-10-28 Mon 03:06]
    - State "TODO"    from              [2019-10-28 Mon 03:06]
            """,
            "Mean Time to Completion in seconds: 60",
            0,
        ],
        ["", "No completed tags found using this configuration", 111],
    ],
)
def test_cli_mtc(org_string, expected_output, exit_code):
    """Test the CLI."""
    runner = CliRunner()
    result = runner.invoke(cli.main, ["mtc", "-F", org_string])
    assert expected_output in result.output
    assert exit_code == result.exit_code
