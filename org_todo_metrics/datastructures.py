from typing import List

Tag = str
TagGroup = List[Tag]
ListTagGroup = List[TagGroup]
