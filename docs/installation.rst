.. highlight:: shell

============
Installation
============


Stable release
--------------

To install org-todo-metrics, run this command in your terminal:

.. code-block:: console

    $ pip install org_todo_metrics

This is the preferred method to install org-todo-metrics, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for org-todo-metrics can be downloaded from the `Gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/reedrichards/org_todo_metrics

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://github.com/reedrichards/org_todo_metrics/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://gitlab.com/reedrichards/org_todo_metrics
.. _tarball: https://gitlab.com/reedrichards/org_todo_metrics/tarball/master
